/// <reference types="cypress" />
//const dataJson = require('../../fixtures/createuser')

describe('Post api request', ()=>{
    let randomName=""
    let newUserPrefix = 'User'
    const newJob = 'Defender'

    it('create user test', () =>{

        var pattern="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz12345678910"
        for(var i=0;i<4;i++)
        randomName=newUserPrefix+=pattern.charAt(Math.floor(Math.random() * pattern.length));
    
        cy.fixture('createuser').then((payload)=>{
            //create user
        cy.request({

            method: 'POST',
            url: '/api/users',
            body:{
                "name": randomName,
                "job": payload.job
            }
        }).then((res) =>{
            cy.log(JSON.stringify(res))
            expect(res.status).to.eq(201)
            expect(res.body).has.property('name', randomName)
            expect(res.body).has.property('job', payload.job)
        }).then((res)=>{
            const newUser=res.body.name
            cy.log('username is '+newUser)
            // update user
            cy.request({
                method: 'PUT',
                url:'/api/users/2',
                body:{
                    "name": newUser,
                    "job": newJob
                }

            }).then((res) =>{
                expect(res.status).to.eq(200)
                expect(res.body).has.property('name', newUser)
                expect(res.body).has.property('job', newJob)
            })
        })
    })
    })
})