/// <reference types="cypress" />

describe('get api user tests', ()=>{

it('get list users', ()=>{

    cy.request({

        method: 'GET',
        url: '/api/users?page=2'

    }).then((res)=>{

        expect(res.status).to.eq(200)
        expect(res.body.data[0].first_name).to.eq('Michael')
    })


})

it('get user by ID', ()=>{

    cy.request({
        method : 'GET',
        url : '/api/users/2'
    }).then((res)=>{

        expect(res.body.data.email).to.eq('janet.weaver@reqres.in')
        expect(res.body.data.last_name).to.eq('Weaver')
    })
})


})